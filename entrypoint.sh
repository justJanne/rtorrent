#!/bin/sh

check_openvpn_running() {
  if [ -z "$(pgrep openvpn)" ]; then
    exit 1
  fi
}

# =============================================================================
# OPENVPN
start_openvpn() {
    if [ -z "${OPENVPN}" ]; then

        # openvpn config
        if [ -n "${OPENVPN_CONFIG_URL}" ] && [ -z "${OPENVPN_CONFIG_PATH}" ]; then
            echo "Downloading and using openvpn config from ${OPENVPN_CONFIG_URL}"
            curl -sLo "/private/conf.openvpn" "${OPENVPN_CONFIG_URL}"
            OPENVPN_CONFIG_PATH="/private/conf.openvpn"
        elif [ -z "${OPENVPN_CONFIG_URL}" ] && [ -z "${OPENVPN_CONFIG_PATH}" ]; then
            echo "One of \$OPENVPN_CONFIG_URL or \$OPENVPN_CONFIG_PATH must be set"
            exit 1
        fi

        # openvpn auth
        if [ -n "${OPENVPN_USER}" ] && [ -n "${OPENVPN_PASS}" ]; then
            printf "%s\n%s" "${OPENVPN_USER}" "${OPENVPN_PASS}" > "/private/auth.txt"
            OPENVPN_AUTH_FILE="/private/auth.txt"
        elif [ -z "${OPENVPN_USER}" ] && [ -z "${OPENVPN_PASS}" ] && [ -z "${OPENVPN_AUTH_FILE}" ]; then
            echo "Warning: neither \$OPENVPN_USER and \$OPENVPN_PASS nor \$OPENVPN_AUTH_FILE are set, auth may fail"
        fi

        # Start openvpn as a daemon and log to a file for us to catch later
        openvpn --daemon --log /private/openvpn.log --client \
            --config /private/conf.openvpn --auth-user-pass "${OPENVPN_AUTH_FILE}"
    fi
}

# =============================================================================
# First run of the script as root user
if [ "$(whoami)" = "root" ]; then
    mkdir -p ${RT_BASE_DIR}log/
    touch ${RT_BASE_DIR}log/rtorrent.log /private/openvpn.log
    chmod 777 ${RT_BASE_DIR}log/rtorrent.log /private/openvpn.log

    rm ${RT_BASE_DIR}.session/rtorrent.lock ${RT_BASE_DIR}.session/rtorrent.pid

    if [ -n "${CREATE_TUN_DEVICE}" ]; then
        mkdir -p /dev/net
        mknod /dev/net/tun c 10 200
        chmod 0666 /dev/net/tun
    fi


    # Get default route gateway and interface and write it to $GW and $INT
    eval $(/sbin/ip route list match 0.0.0.0 | awk '{if($5!="tun0"){print "GW="$3"\nINT="$5; exit}}')

    start_openvpn

    (sleep 4
    if [ -n "${LOCAL_NETWORK}" ]; then
        echo "adding route to local network ${LOCAL_NETWORK} via ${GW} dev ${INT}"
        ip r add "${LOCAL_NETWORK}" via "${GW}" dev "${INT}"
    fi) &

    su abc -c "/entrypoint.sh"

# =============================================================================
# Second run as a non root user
elif [ "$(whoami)" != "root" ]; then
    if [ -n "${CREATE_TUN_DEVICE}" ] && [ ! -f "/dev/net/tun" ]; then
        echo "WARNING: \$CREATE_TUN_DEVICE is set and we are running as a user but /dev/net/tun does not exists.\
              WARNING: /dev/net/tun cannot be create as a regular user. If you are running the container with\
              WARNING: a custom set user id this script could not create the tun device as it was never root."
    fi

    # Start rtorrent as a daemon with the setting in the config file
    # & to actually run it in the background
    rtorrent -n -o "import=${RT_CONFIG_FILE}" &

    (while true
    do
      check_openvpn_running
      sleep 1
    done) &

    # Catch the logs from rtorrent and openvpn
    tail -f "${RT_BASE_DIR}log/rtorrent.log" -f "/private/openvpn.log"
fi

