#+TITLE:   rtorrent + openvpn docker container
#+DATE:    January 30, 2020
#+SINCE:   1.0.0
#+STARTUP: inlineimages nofold

* Table of Contents :TOC_3:noexport:
- [[#description][Description]]
  - [[#maintainers][Maintainers]]
- [[#configuration][Configuration]]
  - [[#rtorrent][rtorrent]]
    - [[#env][ENV]]
  - [[#openvpn][openvpn]]
    - [[#env-1][ENV]]
    - [[#authentication][Authentication]]
    - [[#tun-device][TUN Device]]
- [[#troubleshooting][Troubleshooting]]
  - [[#mknod-devnettun-operation-not-permitted][mknod: /dev/net/tun: Operation not permitted]]

* Description
A Docker image, based on alpine, for rtorrent with an optional openvpn, able to run as an
unprivileged container.

#+NAME: example docker / podman run command
#+BEGIN_SRC shell
podman run -it --rm \
       --cap-add=NET_ADMIN \
       --cap-add=CAP_MKNOD \
       -e OPENVPN_CONFIG_URL="https://downloads.nordcdn.com/configs/files/ovpn_udp/servers/de490.nordvpn.com.udp.ovpn" \
       -e OPENVPN_USER="" \
       -e OPENVPN_PASS="" \
       -p 5000:5000 \
       -p 50000:50000 \
       -v ./data:/data \
       cocainefarm/rtorrent:latest
#+END_SRC

** Maintainers
+ [[https://gitlab.com/audron][@audron]] (Author)

* Configuration
All configuration can be done through environment variables but it's possible to
mount in a openvpn configuration file and credentials file

** rtorrent
*** ENV
+ *RT\_BASE\_DIR*: base directory for rtorrent files like downloads and .torrent
  files. A trailing slash (/) is important ( default "/data/" )
+ *RT\_TRACKER\_UDP*: Enable or disable support for connecting to UDP trackers.
  ( default "yes" )
+ *RT\_DHT\_MODE*: Enable or disable DHT trackerless torrents. ( default "disable" )
+ *RT\_DHT\_PORT*: Port for trackerless torrents. ( default 49160 )
+ *RT\_PROTO\_PEX*: Enable or disable DHT trackerless torrents. ( default "no" )
+ *RT\_MAX\_UP*: Max upload bandwidth per torrent. ( default 100 )
+ *RT\_MAX\_UP\_GLOBAL*: Max upload bandwidth globally. ( default 250 )
+ *RT\_MIN\_PEERS*: Minimum amount of Peers while downloading. ( default 20 )
+ *RT\_MAX\_PEERS*: Maximum amount of Peers while downloading. ( default 60 )
+ *RT\_MIN\_PEERS\_SEED*: Minimum amount of Peers while seeding. ( default 30 )
+ *RT\_MAX\_PEERS\_SEED*: Maximum amount of Peers while seeding. ( default 80 )
+ *RT\_TRACKERS\_WANT*: Number of wanted trackers. ( default 80 )
+ *RT\_MEMORY\_MAX*: Maximum mount of memory to use. ( default "1800M" )
+ *RT\_DAEMON*: Run as a daemon and disable the UI. ( default "true" )
+ *RT\_XMLRPC\_BIND*: Bind IP for xmlrpc. ( default "0.0.0.0" )
+ *RT\_XMLRPC\_PORT*: Bind port for xmlrpc. ( default "5000" )
+ *RT\_CONFIG\_FILE*: Location of the config file. ( default "/.rtorrent.rc" )

** openvpn
*** ENV
+ *CREATE\_TUN\_DEVICE*: Create /dev/net/tun inside the container. See [[TUN
  Device]]. Empty is false. ( default "" )
+ *OPENVPN\_CONFIG\_URL*: URL from which to download and use an openvpn client
  configuration file. ( default "" )
+ *OPENVPN\_CONFIG_PATH*: Sets where to find the openvpn client config when e.g.
  mounting in your own config. ( default "" )
+ *OPENVPN\_USER*: User for openvpn login. ( default "" )
+ *OPENVPN\_PASS*: Password for openvpn login. ( default "" )
+ *OPENVPN\_AUTH\_FILE*: Path to a file that contains the openvpn credentials. User on
  first line, password on second line. See [[Authentication]]. ( default "" )

*** Authentication
For authenticating against openvpn you can either provide a user and password
via the environment variables, or mount in a file containing them.

The environment variables will be written to ~/private/auth.txt~ and that file
pass to openvpn as ~--auth-user-pass~

When mounting your own file and setting *OPENVPN\_AUTH\_FILE* to it's path the
environment variables will be ignored.

*** TUN Device
Openvpn needs a tun device in ~/dev/net/tun~ to work correctly.
You have to options to get that in a container:

+ Run the container as privileged and mount in ~/dev/net/tun~ from the host.
+ Give the container the ~NET_ADMIN~ and ~CAP_MKNOD~ capabilities and set
  *CREATE\_TUN\_DEVICE* to have ~/dev/net/tun~ create inside the container.

* Troubleshooting
** mknod: /dev/net/tun: Operation not permitted
The container is running with the ~NET_ADMIN~ and ~CAP_MKNOD~ capabilities but
as a rootless container like podman. Creating the tun in the container will not
work with this setup. Run the container with ~--privileged~ and the capabilities
and the ~/dev/net/tun~ device will be ready to use in the container.
